package dev.dt.mankala.web.ui;

import dev.dt.mankala.api.data.Player;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@UIScope
@SpringComponent
public class PitComponent extends VerticalLayout {
    private final Button pit = new Button();
    private final Label pitLabel = new Label();
    private Player player;
    private int pitId;

    public PitComponent() {
        pit.addClassNames("pit");
        pitLabel.addClassNames("pit-label");
        add(pitLabel, pit);
        setAlignItems(Alignment.CENTER);
    }

    public void setValue(int value) {
        pit.setText(String.valueOf(value));
        pit.setEnabled(value>0);
    }

    public void setEventBus(ComponentEventListener<ClickEvent<Button>> listener) {
        pit.addClickListener(listener);
    }

    public int getPitId() {
        return pitId;
    }

    public void setPitId(int pitId) {
        this.pitId = pitId;
        pitLabel.setText(String.valueOf(pitId));
        pit.setId(player.getName() + "." + pitId);
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
