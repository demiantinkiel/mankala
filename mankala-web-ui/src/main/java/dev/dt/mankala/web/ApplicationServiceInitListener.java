package dev.dt.mankala.web;

import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinServiceInitListener;

import static java.lang.System.setProperty;

public class ApplicationServiceInitListener implements VaadinServiceInitListener {

    @Override
    public void serviceInit(ServiceInitEvent e) {
        setProperty("vaadin.i18n.provider", SimpleI18NProvider.class.getName());
    }

}
