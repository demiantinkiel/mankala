package dev.dt.mankala.web.ui;

import dev.dt.mankala.api.data.Player;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.EventData;
import com.vaadin.flow.component.button.Button;

@DomEvent("click")
public class MoveEvent extends ComponentEvent<Button> {
    public MoveEvent(Button source, boolean fromClient, @EventData("event.player") Player player, @EventData("event.button") int pitId) {
        super(source, fromClient);

    }
}
