package dev.dt.mankala.web.middleware;

import dev.dt.mankala.api.data.Game;
import dev.dt.mankala.web.GameChangeEvent;
import dev.dt.mankala.web.MankalaWebConfig;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@SpringComponent
public class GameClient implements ComponentEventListener<ClickEvent<Button>> {
    private final String gameURI = "/game";
    private final RestTemplate template;
    Game currentGame;
    private final ApplicationEventPublisher eventPublisher;

    public GameClient(RestTemplateBuilder builder, MankalaWebConfig config, ApplicationEventPublisher eventPublisher) {
        this.template = builder.rootUri(config.getBackendUrl()).build();
        this.eventPublisher = eventPublisher;
    }

    public void newGame(List<String> players, String startingPlayer) {
        template.postForObject(gameURI + "?starter={startingPlayer}", players, Game.class, startingPlayer);
        refreshGame();
    }

    public void makeMove(String pit, String player) {
        currentGame = template.patchForObject(gameURI + "?pit={pit}", player, Game.class, pit);
        eventPublisher.publishEvent(new GameChangeEvent(this, currentGame));
    }

    public void stopGame() {
        template.delete(gameURI);
        refreshGame();
    }

    public Game getCurrentGame() {
        if (currentGame == null) {
            refreshGame();
        }
        return currentGame;
    }

    @Override
    public void onComponentEvent(ClickEvent<Button> buttonClickEvent) {
        buttonClickEvent.getSource().getId().ifPresent(id -> {
            String player = id.split("\\.")[0];
            String pit = id.split("\\.")[1];
            makeMove(pit, player);
        });
    }

    private void refreshGame() {
        currentGame = template.getForObject(gameURI, Game.class);
    }
}
