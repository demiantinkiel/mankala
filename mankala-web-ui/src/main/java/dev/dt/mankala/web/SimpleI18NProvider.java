package dev.dt.mankala.web;

import com.vaadin.flow.i18n.I18NProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableList;
import static java.util.Locale.ENGLISH;
import static java.util.ResourceBundle.getBundle;

@Slf4j
@Component
public class SimpleI18NProvider implements I18NProvider {

    public static final String RESOURCE_BUNDLE_NAME = "vaadinapp";
    public static final Locale DUTCH = new Locale("nl", "NL");

    private static final ResourceBundle RESOURCE_BUNDLE_EN = getBundle(RESOURCE_BUNDLE_NAME, ENGLISH);
    private static final ResourceBundle RESOURCE_BUNDLE_NL = getBundle(RESOURCE_BUNDLE_NAME, DUTCH);
    private static final List providedLocales = unmodifiableList(asList(ENGLISH, DUTCH));

    @Override
    public List<Locale> getProvidedLocales() {
        return providedLocales;
    }

    @Override
    public String getTranslation(String key, Locale locale, Object... objects) {
        ResourceBundle resourceBundle = RESOURCE_BUNDLE_EN;
        if (DUTCH.getLanguage().equals(locale.getLanguage())) {
            resourceBundle = RESOURCE_BUNDLE_NL;
        }

        if (!resourceBundle.containsKey(key)) {
            log.info("missing resource key (i18n) " + key);
            return key + " - " + locale;
        } else {
            return (resourceBundle.containsKey(key)) ? resourceBundle.getString(key) : key;
        }
    }
}
