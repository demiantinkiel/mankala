package dev.dt.mankala.web.ui;

import dev.dt.mankala.web.MankalaWebConfig;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.i18n.LocaleChangeObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@Route(value = "help", layout = MainView.class)
@PageTitle("Help")
@SpringComponent
public class HelpView extends VerticalLayout implements LocaleChangeObserver {

    @Autowired
    AboutDialog aboutDialog;
    @Autowired
    MankalaWebConfig config;
    TextArea howToPlay;
    Anchor tips;
    Anchor rules;
    Button aboutButton;

    public HelpView() {
        howToPlay = new TextArea();
        howToPlay.setValue("");
        howToPlay.setReadOnly(true);
        howToPlay.setWidthFull();
        howToPlay.setMaxHeight("50%");
        howToPlay.addClassName("info");

        tips = new Anchor();
        tips.setText("Strategy tips");
        tips.setTarget("_blank");

        rules = new Anchor();
        rules.setText("Rules");
        rules.setTarget("_blank");

        aboutButton = new Button("About", c -> aboutDialog.open());

        add(howToPlay, rules, tips, aboutButton);

        setPadding(true);
        setMargin(true);
    }

    @Override
    public void localeChange(LocaleChangeEvent event) {
        howToPlay.setLabel(getTranslation("howToPlay-label"));
        howToPlay.setValue(getTranslation("howToPlay-content"));
        tips.setText(getTranslation("tips-text"));
        rules.setText(getTranslation("rules-text"));
        aboutButton.setText(getTranslation("about-label"));
    }

    @PostConstruct
    private void init() {
        tips.setHref(config.getTipUrl());
        rules.setHref(config.getRulesUrl());
    }


}
