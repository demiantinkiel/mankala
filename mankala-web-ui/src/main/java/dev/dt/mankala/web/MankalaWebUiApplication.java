package dev.dt.mankala.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import static java.lang.System.setProperty;

@SpringBootApplication
@EnableScheduling
public class MankalaWebUiApplication {

    public static void main(String[] args) {
        setProperty("vaadin.i18n.provider", SimpleI18NProvider.class.getName());
        SpringApplication.run(MankalaWebUiApplication.class, args);
    }

}
