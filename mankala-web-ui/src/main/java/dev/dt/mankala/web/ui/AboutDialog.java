package dev.dt.mankala.web.ui;

import dev.dt.mankala.web.MankalaWebConfig;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.i18n.LocaleChangeObserver;
import com.vaadin.flow.spring.annotation.SpringComponent;

@SpringComponent
public class AboutDialog extends Dialog implements LocaleChangeObserver {
    final MankalaWebConfig config;

    @Id("aboutContentText")
    Text aboutText = new Text("");

    @Id("aboutAuthorText")
    Anchor authorText = new Anchor();

    public AboutDialog(MankalaWebConfig config) {
        this.config = config;
        authorText.setText("Demian Tinkiel");
        authorText.setHref(config.getAuthorProfile());
        authorText.setTarget("_blank");

        VerticalLayout layout = new VerticalLayout();
        layout.add(new H1("Mankala game"),
                aboutText,
                authorText,
                new Button("ok!", buttonClickEvent -> this.close()));
        add(layout);
        setModal(true);
        setDraggable(false);
        setResizable(false);
    }

    @Override
    public void localeChange(LocaleChangeEvent event) {
        aboutText.setText(getTranslation("about-msg"));
    }
}
