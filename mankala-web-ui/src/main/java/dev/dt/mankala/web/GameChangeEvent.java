package dev.dt.mankala.web;

import dev.dt.mankala.api.data.Game;
import org.springframework.context.ApplicationEvent;

public class GameChangeEvent extends ApplicationEvent {
    Game game;
    public GameChangeEvent(Object source, Game game) {
        super(source);
        this.game=game;
    }

    public Game getGame() {
        return game;
    }
}
