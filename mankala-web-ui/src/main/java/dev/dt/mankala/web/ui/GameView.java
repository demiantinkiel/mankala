package dev.dt.mankala.web.ui;

import dev.dt.mankala.api.data.Game;
import dev.dt.mankala.api.data.Player;
import dev.dt.mankala.web.GameChangeEvent;
import dev.dt.mankala.web.middleware.GameClient;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.i18n.LocaleChangeObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.context.event.EventListener;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Arrays;
import java.util.stream.Collectors;

@Route(value = "game", layout = MainView.class)
@PageTitle("Game")
@SpringComponent
public class GameView extends HorizontalLayout implements LocaleChangeObserver {
    RadioButtonGroup<String> currentPlayer;
    Button stopGame;
    Button newGame;
    GameClient client;
    BoardComponent board;


    GameView(GameClient client) {
        this.client = client;
        currentPlayer = new RadioButtonGroup<>();
        currentPlayer.setLabel("Current Player");
        currentPlayer.setReadOnly(true);
        board = new BoardComponent(client);
        newGame = new Button("new", c -> {
            try {
                client.newGame(Arrays.asList("player1", "player2"), "player1");
            } catch (HttpClientErrorException e) {
                if (e.getStatusCode().is4xxClientError()) {
                    Notification.show(e.getStatusText());
                } else {
                    Notification.show("Server error, please try again later or contact system administrator");
                }
            } finally {
                loadUI();
            }
        });
        stopGame = new Button("stop", c -> {
            client.stopGame();
            loadUI();
        });
        VerticalLayout controls = new VerticalLayout(currentPlayer, newGame, stopGame);
        controls.addClassName("form-control");
        controls.setSizeUndefined();
        loadUI();
        add(controls, board);
        setMargin(true);
        setPadding(true);
    }

    @EventListener
    public void handleGameEvent(GameChangeEvent event) {
        loadUI();
    }

    @Override
    public void localeChange(LocaleChangeEvent event) {
        currentPlayer.setLabel(getTranslation("currentPlayer-label"));
        newGame.setText(getTranslation("newgame-label"));
        stopGame.setText(getTranslation("stop-label"));
    }

    private void loadUI() {
        Game game = client.getCurrentGame();
        if (game != null) {
            currentPlayer.setItems(game.getPlayers().stream().map(Player::getName).collect(Collectors.toList()));
            currentPlayer.setValue(game.getCurrentPlayer().getName());
            stopGame.setEnabled(true);
            newGame.setEnabled(false);
            board.update(game);
        } else {
            stopGame.setEnabled(false);
            newGame.setEnabled(true);
            board.update(null);
        }
    }
}
