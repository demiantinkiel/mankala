package dev.dt.mankala.web.ui;

import dev.dt.mankala.api.data.Game;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.HasEnabled;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.i18n.LocaleChangeObserver;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

@UIScope
@SpringComponent
public class BoardComponent extends VerticalLayout implements LocaleChangeObserver {
    private final ComponentEventListener<ClickEvent<Button>> listener;
    private final Label gameState;
    private String hasWonMsg;
    private String noGameMsg;

    public BoardComponent(ComponentEventListener<ClickEvent<Button>> listener) {
        this.listener = listener;
        this.gameState = new Label();
        gameState.setVisible(false);
        gameState.setWidthFull();
        add(gameState);
    }

    public void update(Game game) {
        if (game != null) {
            if (!game.isEnded()) {
                removeAll();
                game.getPlayers().forEach(player -> {
                    HorizontalLayout playerBoard = new HorizontalLayout();
                    playerBoard.setClassName("boxed");
                    playerBoard.setId(player.getName() + ".board");
                    AtomicInteger pitCounter = new AtomicInteger(player.getStartPit());
                    Arrays.stream(game.getPitsForPlayer(player)).forEachOrdered(i -> {
                        PitComponent pit = new PitComponent();
                        pit.setPlayer(player);
                        pit.setValue(i);
                        pit.setEventBus(listener);
                        pit.setPitId(pitCounter.getAndIncrement());
                        playerBoard.add(pit);
                    });
                    Label playerMankala = new Label(String.valueOf(game.getMancalaForPlayer(player)));
                    playerMankala.addClassNames("mankala");
                    playerMankala.setHeightFull();
                    playerBoard.add(playerMankala);
                    playerBoard.addClassNames("playerBoard");
                    if(player.equals(game.getCurrentPlayer())){
                        playerBoard.setEnabled(true);
                        playerBoard.addClassNames("focus");
                    }else {
                        playerBoard.setEnabled(false);
                        playerBoard.removeClassNames("focus");
                    }
                    add(playerBoard);
                });
            } else {
                getChildren().filter(c -> c instanceof HasEnabled).map(c -> (HasEnabled) c).forEach(c -> c.setEnabled(false));
                getChildren().filter(c -> c instanceof HasStyle).map(c -> (HasStyle) c).forEach(c -> c.removeClassNames("focus"));
                String msg = game.getWinner().getName() + hasWonMsg;
                getUI().ifPresent(ui -> Notification.show(msg));
                this.gameState.setVisible(true);
                this.gameState.setText(msg);
                add(gameState);
            }
        } else {
            removeAll();
            this.gameState.setVisible(true);
            this.gameState.setText(noGameMsg);
            add(gameState);
        }
    }

    @Override
    public void localeChange(LocaleChangeEvent event) {
        hasWonMsg = getTranslation("hasWon-msg");
        noGameMsg = getTranslation("noGame-msg");
    }
}
