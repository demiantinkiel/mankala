package dev.dt.mankala.web.ui;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.i18n.LocaleChangeObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;

import java.util.Locale;

@SpringComponent
@PageTitle("Settings")
@Route(value = "settings", layout = MainView.class)
public class SettingsView extends VerticalLayout implements LocaleChangeObserver {
    Select<String> language;

    SettingsView() {
        language = new Select<>("EN", "NL");
        language.addValueChangeListener(event -> {
            if (event.getValue().equalsIgnoreCase("NL")) {
                getUI().ifPresent(ui -> ui.setLocale(new Locale("NL", "nl"))); //FIXME java doesn't have dutch locale?!
            }
            getUI().ifPresent(ui -> ui.setLocale(Locale.forLanguageTag(event.getValue())));
        });
        add(language);
        setPadding(true);
        setMargin(true);
    }

    @Override
    public void localeChange(LocaleChangeEvent event) {
        language.setLabel(getTranslation("language-label", event.getLocale()));
        language.setValue(event.getLocale().getLanguage().toUpperCase());
    }
}
