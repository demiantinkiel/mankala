package dev.dt.mankala.backend.rest;

import dev.dt.mankala.api.data.Game;
import dev.dt.mankala.api.data.Player;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import static dev.dt.mankala.backend.service.GameService.GAME_EXISTS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class basicRestIT {
    @Autowired
    MockMvc mvc;

    @Autowired
    ObjectMapper mapper;

    static Stream<Arguments> newGameArgs() {
        return Stream.of(
                arguments(new HashSet<>(Arrays.asList("player1", "player2")), "player1", HttpStatus.OK),
                arguments(new HashSet<>(Arrays.asList("player1", "player2")), "player3", HttpStatus.BAD_REQUEST),
                arguments(new HashSet<>(Collections.singletonList("player1")), "player1", HttpStatus.BAD_REQUEST)
        );
    }

    @ParameterizedTest(name = "#{index} - new game for {0}, starting {1} should result in {2}")
    @MethodSource("newGameArgs")
    void newGame(Set<String> players, String starter, HttpStatus expectedStatus) throws Exception {
        mvc.perform(post("/game")
                .contentType(MediaType.APPLICATION_JSON)
                .param("starter", starter)
                .content(mapper.writeValueAsString(players)))
                .andExpect(status().is(expectedStatus.value()))
        ;
    }

    @Test
    void newGame_AlreadyStarted() throws Exception {
        mvc.perform(post("/game")
                .contentType(MediaType.APPLICATION_JSON)
                .param("starter", "player1")
                .content(mapper.writeValueAsString(Arrays.asList("player1", "player2")))
        ).andExpect(status().isOk());

        mvc.perform(post("/game")
                .contentType(MediaType.APPLICATION_JSON)
                .param("starter", "player1")
                .content(mapper.writeValueAsString(Arrays.asList("player1", "player2"))))
                .andExpect(status().isConflict())
                .andExpect(content().string(GAME_EXISTS))
        ;
    }

    @Test
    void basicFlow() throws Exception {
        Game game;

        mvc.perform(post("/game")
                .contentType(MediaType.APPLICATION_JSON)
                .param("starter", "player1")
                .content(mapper.writeValueAsString(Arrays.asList("player1", "player2"))))
                .andDo(log())
                .andExpect(status().isOk());

        game = toGameObject(mvc.perform(get("/game"))
                .andDo(log())
                .andExpect(status().isOk())
                .andReturn().getResponse());
        Player player1 = game.getPlayers().stream().filter(player -> player.getName().equals("player1")).findFirst().orElseThrow(Exception::new);
        Player player2 = game.getPlayers().stream().filter(player -> player.getName().equals("player2")).findFirst().orElseThrow(Exception::new);
        int p1 = player1.getStartPit();
        int p2 = player2.getStartPit();

        while (!game.isEnded()) {
            int pit;
            if (game.getCurrentPlayer().equals(player1)) {
                pit = p1;
                p1 = p1 + 1 >= player1.getEndPit() ? player1.getStartPit() : p1 + 1;
            } else {
                pit = p2;
                p2 = p2 + 1 >= player2.getEndPit() ? player2.getStartPit() : p2 + 1;
            }

            while (game.getBoard()[pit] == 0) {
                pit = pit + 1 >= game.getCurrentPlayer().getEndPit() ? game.getCurrentPlayer().getStartPit() : pit + 1;
            }

            game = toGameObject(mvc.perform(patch("/game")
                    .contentType(MediaType.APPLICATION_JSON)
                    .param("pit", String.valueOf(pit))
                    .content(game.getCurrentPlayer().getName()))
                    .andDo(log())
                    .andExpect(status().isOk())
                    .andReturn().getResponse()
            );
        }
        assertThat(game.isEnded()).isTrue();
        assertThat(game.getWinner().getName()).isEqualTo("player1");
        mvc.perform(delete("/game"))
                .andDo(log())
                .andExpect(status().isOk());
    }

    private Game toGameObject(MockHttpServletResponse response) throws UnsupportedEncodingException, JsonProcessingException {
        return mapper.readValue(response.getContentAsString(), Game.class);
    }
}
