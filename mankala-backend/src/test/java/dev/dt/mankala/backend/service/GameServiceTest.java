package dev.dt.mankala.backend.service;

import dev.dt.mankala.api.data.Game;
import dev.dt.mankala.api.data.Player;
import dev.dt.mankala.api.error.MankalaException;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import static dev.dt.mankala.backend.service.GameService.GAME_EXISTS;
import static dev.dt.mankala.backend.service.GameService.INCORRECT_NUM_PLAYERS;
import static dev.dt.mankala.backend.service.GameService.INVALID_STARTING_PIT;
import static dev.dt.mankala.backend.service.GameService.NOT_PLAYER_TURN;
import static dev.dt.mankala.backend.service.GameService.NO_GAME_IN_PROGRESS;
import static dev.dt.mankala.backend.service.GameService.UNKNOWN_PLAYER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Slf4j
class GameServiceTest {
    GameService gameService;
    Player player1;
    Player player2;
    Set<Player> players;

    @BeforeEach
    void setUp() {
        gameService = new GameService(6, 7);
        player1 = new Player("player1");
        player2 = new Player("player2");
        players = new LinkedHashSet<>(Arrays.asList(player1, player2));
    }

    @Test
    void startNewGame() {
        Player unknown = new Player("John Doe");

        Exception exception = assertThrows(MankalaException.class, () -> gameService.startNewGame(Collections.singleton(player1), player1));
        assertThat(exception.getMessage()).contains(INCORRECT_NUM_PLAYERS);

        exception = assertThrows(MankalaException.class, () -> gameService.startNewGame(players, unknown));
        assertThat(exception.getMessage()).contains(UNKNOWN_PLAYER);

        Game game = gameService.startNewGame(players, player1);
        logBoard(game, "new game");
        assertThat(game.getCurrentPlayer()).isEqualTo(player1);
        assertThat(game.getBoard().length).isEqualTo(14);
        assertThat(game.getPlayers()).first().extracting(Player::getStartPit).isEqualTo(0);
        assertThat(game.getPlayers()).first().extracting(Player::getEndPit).isEqualTo(6);
        assertThat(game.getPlayers()).last().extracting(Player::getStartPit).isEqualTo(7);
        assertThat(game.getPlayers()).last().extracting(Player::getEndPit).isEqualTo(13);

        int[] player1Pits = Arrays.copyOfRange(game.getBoard(), player1.getStartPit(), player1.getEndPit() - 1);
        int[] player2Pits = Arrays.copyOfRange(game.getBoard(), player2.getStartPit(), player2.getEndPit() - 1);
        assertThat(Arrays.stream(player1Pits)).allMatch(stones -> stones == gameService.stonesPerPit);
        assertThat(game.getBoard()[player1.getEndPit()]).isEqualTo(0);
        assertThat(Arrays.stream(player2Pits)).allMatch(stones -> stones == gameService.stonesPerPit);
        assertThat(game.getBoard()[player2.getEndPit()]).isEqualTo(0);

        exception = assertThrows(MankalaException.class, () -> gameService.startNewGame(players, player2));
        assertThat(exception.getMessage()).contains(GAME_EXISTS);
    }

    @Test
    void stopGame() {
        gameService.startNewGame(players, player1);
        gameService.stopGame();
        assertThat(gameService.getCurrentGame()).isNull();
    }

    @Test
    void getCurrentGame() {
        gameService.startNewGame(players, player1);
        assertThat(gameService.getCurrentGame()).isNotNull();
    }

    @Test
    void makeMove() {
        Game game = gameService.startNewGame(players, player1);
        logBoard(game, "Start");
        game = gameService.makeMove(player1, 5);
        logBoard(game, player1.getName() + "move 1");
        assertThat(game.getCurrentPlayer()).isEqualTo(player2);
        assertThat(game.getBoard()[5]).isEqualTo(0);
        assertThat(game.getBoard()[player1.getEndPit()]).isEqualTo(1);
        int[] remainingPlayablePits = Arrays.copyOfRange(game.getBoard(), player2.getStartPit(), player2.getEndPit() - 1);
        assertThat(remainingPlayablePits).containsOnly(gameService.stonesPerPit + 1);
        assertThat(game.getBoard()[player2.getEndPit() - 1]).isEqualTo(gameService.stonesPerPit);

        game = gameService.makeMove(player2, 7);
        logBoard(game, player2.getName() + "move 1");
        assertThat(game.getCurrentPlayer()).isEqualTo(player1);
        assertThat(game.getBoard()[7]).isEqualTo(0);
    }

    @Test
    void makeMove_win() {
        gameService = new GameService(1, 3);
        Game game = gameService.startNewGame(players, player1);
        logBoard(game, "Initial");
        game = gameService.makeMove(player1, 0);
        logBoard(game, "player1 move1");
        game = gameService.makeMove(player2, 3);
        logBoard(game, "player2 move1");
        game = gameService.makeMove(player1, 1);
        logBoard(game, "player1 move2");
        assertThat(game.isEnded()).isTrue();
        assertThat(game.getWinner()).isEqualTo(player2);
    }

    @Test
    void makeMove_goAgain() {
        gameService = new GameService(2, 3);
        Game game = gameService.startNewGame(players, player1);
        logBoard(game, "Initial");
        game = gameService.makeMove(player1, 0);
        logBoard(game, "player1 move1");
        assertThat(game.getCurrentPlayer()).isEqualTo(player1);
    }

    @Test
    void makeMove_capture() {
        gameService = new GameService(2, 4);
        gameService.setGameEndCheck(false);
        Game game = gameService.startNewGame(players, player1);
        logBoard(game, "Initial");
        game = gameService.makeMove(player1, 0);
        logBoard(game, "player1 move1");
        game = gameService.makeMove(player2, 4);
        logBoard(game, "player2 move1");
        game = gameService.makeMove(player1, 1);
        logBoard(game, "player1 move2");
        game = gameService.makeMove(player2, 6);
        logBoard(game, "player2 move2");
        game = gameService.makeMove(player1, 1);
        logBoard(game, "player1 move3");
        game = gameService.makeMove(player2, 4);
        logBoard(game, "player2 move3");
        game = gameService.makeMove(player1, 0);
        logBoard(game, "player1 move4");
        assertThat(game.getBoard()[1]).isEqualTo(5);
        assertThat(game.getBoard()[5]).isEqualTo(0);
    }

    private void logBoard(Game game, String label) {
        StringBuilder boardLog = new StringBuilder("\n\t\t  ");
        for (int i = 0; i < gameService.pitsPerPlayer - 1; i++) {
            boardLog.append(i).append(", ");
        }
        boardLog.append("M\n");

        game.getPlayers().forEach(player ->
                boardLog.append(String.format("%s: %s\n", player.getName(),
                        Arrays.toString(Arrays.copyOfRange(game.getBoard(), player.getStartPit(), player.getEndPit() + 1))
                )));

        log.info("{} {}", label, boardLog.toString());
    }

    @Test
    void makeMove_validateInput() {
        Exception exception = assertThrows(MankalaException.class, () -> gameService.makeMove(player2, 8));
        assertThat(exception.getMessage()).contains(NO_GAME_IN_PROGRESS);

        gameService.startNewGame(players, player1);

        exception = assertThrows(MankalaException.class, () -> gameService.makeMove(new Player("joe"), 8));
        assertThat(exception.getMessage()).contains(UNKNOWN_PLAYER);

        exception = assertThrows(MankalaException.class, () -> gameService.makeMove(player2, 8));
        assertThat(exception.getMessage()).contains(NOT_PLAYER_TURN);

        exception = assertThrows(MankalaException.class, () -> gameService.makeMove(player1, 6));
        assertThat(exception.getMessage()).contains(INVALID_STARTING_PIT);
    }
}
