package dev.dt.mankala.backend.rest;

import dev.dt.mankala.api.data.Game;
import dev.dt.mankala.api.data.Player;
import dev.dt.mankala.backend.service.GameService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Min;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/game")
public class GameController {

    final GameService gameService;

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @PostMapping
    @Operation(summary = "Starts a new game")
    public Game startNewGame(@RequestBody @Parameter(description = "Name of players") @Min(2) Set<String> playerNames,
                             @RequestParam @Parameter(description = "Starting player") String starter) {
        Set<Player> players = playerNames.stream().map(Player::new).collect(Collectors.toCollection(LinkedHashSet::new));
        return gameService.startNewGame(players, new Player(starter));
    }

    @DeleteMapping
    @Operation(summary = "Stops the current game")
    public void stopGame() {
        gameService.stopGame();
    }

    @GetMapping
    @Operation(summary = "Gets the current game")
    public Game getCurrentGame() {
        return gameService.getCurrentGame();
    }

    @PatchMapping
    @Operation(summary = "Makes a move for a player")
    public Game makeMove(@RequestBody @Parameter(description = "Name of player to make move") String player,
                         @RequestParam @Parameter(description = "Pit from which to start move") int pit) {
        return gameService.makeMove(new Player(player), pit);
    }


}
