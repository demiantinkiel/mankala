package dev.dt.mankala.backend.service;

import dev.dt.mankala.api.data.Game;
import dev.dt.mankala.api.data.Player;
import dev.dt.mankala.api.error.MankalaException;
import dev.dt.mankala.backend.MancalaGameConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static dev.dt.mankala.api.error.MankalaException.ErrorType.BAD_INPUT;
import static dev.dt.mankala.api.error.MankalaException.ErrorType.BAD_STATE;

@Service
@Slf4j
@Configurable
public class GameService {

    public static final String GAME_EXISTS = "There is already a game in progress";
    public static final String INCORRECT_NUM_PLAYERS = "Cannot start with this amount of players (min 2 players)";
    public static final String UNKNOWN_PLAYER = "Unknown player";
    public static final String NO_GAME_IN_PROGRESS = "No game in progress";
    public static final String NOT_PLAYER_TURN = "It is not the turn of that player";
    public static final String INVALID_STARTING_PIT = "Invalid starting pit";

    public final int stonesPerPit;
    public final int pitsPerPlayer;
    public boolean gameEndCheck = true;

    Game game;

    @Autowired
    public GameService(MancalaGameConfig config) {
        this.stonesPerPit = config.getStonesPerPit();
        this.pitsPerPlayer = config.getPitsPerPlayer();
    }

    public GameService(int stonesPerPit, int pitsPerPlayer) {
        this.stonesPerPit = stonesPerPit;
        this.pitsPerPlayer = pitsPerPlayer;
    }

    public Game startNewGame(Set<Player> players, Player starter) {
        log.info("Starting new game");
        if (game != null) {
            throw new MankalaException(GAME_EXISTS, null, BAD_STATE);
        }
        if (!players.contains(starter)) {
            throw new MankalaException(UNKNOWN_PLAYER, null, BAD_INPUT);
        }
        if (players.size() < 2) {
            throw new MankalaException(INCORRECT_NUM_PLAYERS, null, BAD_INPUT);
        }
        log.debug("Starting new game for players {}. Ready {}", players.stream().map(Player::getName).collect(Collectors.toList()), starter.getName());

        game = new Game();

        int[] board = new int[players.size() * pitsPerPlayer];

        AtomicInteger count = new AtomicInteger();
        players.forEach(player -> {
            player.setStartPit(count.get());
            player.setEndPit(count.get() + pitsPerPlayer - 1);
            Arrays.fill(board, player.getStartPit(), player.getEndPit(), stonesPerPit);

            count.getAndAdd(pitsPerPlayer);
        });
        game.setBoard(board);
        game.setPlayers(players);
        game.setCurrentPlayer(players.stream().filter(player -> player.equals(starter)).findFirst().orElse(null));
        return game;
    }

    public void stopGame() {
        log.debug("Stopping game {}", game);
        game = null;
    }

    public Game getCurrentGame() {
        return game;
    }

    public Game makeMove(Player player, int startingPit) {
        if (game == null) {
            throw new MankalaException(NO_GAME_IN_PROGRESS, null, BAD_STATE);
        }
        if (game.isEnded()) {
            throw new MankalaException(NO_GAME_IN_PROGRESS, null, BAD_STATE);
        }
        if (!game.getPlayers().contains(player)) {
            throw new MankalaException(UNKNOWN_PLAYER, null, BAD_INPUT);
        }
        if (!Objects.equals(game.getCurrentPlayer(), player)) {
            throw new MankalaException(NOT_PLAYER_TURN, null, BAD_INPUT);
        }
        if (startingPit > game.getBoard().length || !pitBelongsToPlayer(startingPit, game.getCurrentPlayer()) || game.getBoard()[startingPit] == 0) {
            throw new MankalaException(INVALID_STARTING_PIT, null, BAD_INPUT);
        }

        player = game.getCurrentPlayer();
        log.debug("{} is making a move from pit {}", player, startingPit);

        int stonesInHand = game.getBoard()[startingPit];
        Player nextPlayer = getNextPlayer(game.getCurrentPlayer());
        int[] board = game.getBoard();
        board[startingPit] = 0;

        for (int pit = (startingPit + 1) % game.getBoard().length; pit <= game.getBoard().length; pit++) {

            if (pit == nextPlayer.getEndPit()) {
                log.warn("Skipping pit {}", pit);
                pit = (pit + 1) % game.getBoard().length;
            }
            pit = pit % game.getBoard().length;
            log.trace("Putting a stone to pit {} ({} stones)", pit, game.getBoard()[pit]);
            board[pit]++;
            log.trace("pit {} now has {} stones", pit, game.getBoard()[pit]);
            stonesInHand--;
            log.trace("{} stones left in hand\n", stonesInHand);

            if (stonesInHand == 0) {
                if (pitBelongsToPlayer(pit, player) && game.getBoard()[pit] == 1) {
                    int capturedPit = (nextPlayer.getStartPit() + (player.getEndPit() - pit)) - 1;
                    int capturedStones = board[capturedPit];
                    board[capturedPit] = 0;
                    board[pit] += capturedStones;
                    log.trace("Player {} captured {} stones from pit {}", player.getName(), capturedStones, capturedPit);
                }
                if (pit == game.getCurrentPlayer().getEndPit()) {
                    game.setCurrentPlayer(player);
                } else {
                    game.setCurrentPlayer(nextPlayer);
                }
                break;
            }
        }
        game.setBoard(board);
        log.debug("Move end. Ready {}", game.getCurrentPlayer());
        checkGameEnd();
        return game;
    }

    public void setGameEndCheck(boolean gameEndCheck) {
        this.gameEndCheck = gameEndCheck;
    }

    private void checkGameEnd() {
        for (Player player : game.getPlayers()) {
            if (Arrays.stream(game.getPitsForPlayer(player)).allMatch(p -> p == 0)) {
                endgame();
            }
        }
    }

    private void endgame() {
        if (!gameEndCheck)
            return;
        Player winner = game.getCurrentPlayer();

        for (Player player : game.getPlayers()) {
            game.remainingStonesToMancala(player);
            if (game.getMancalaForPlayer(player) > game.getMancalaForPlayer(winner)) {
                winner = player;
            }
        }
        game.setWinner(winner);
        game.setEnded(true);
        log.info("Game has ended");
        log.debug("winner: {}", game.getWinner());
    }

    private boolean pitBelongsToPlayer(int startingPit, Player player) {
        return startingPit >= player.getStartPit() && startingPit <= player.getEndPit() - 1;
    }

    private Player getNextPlayer(Player current) {
        Set<Player> players = new LinkedHashSet<>(game.getPlayers());
        players.remove(current);
        return players.iterator().next();
    }
}
