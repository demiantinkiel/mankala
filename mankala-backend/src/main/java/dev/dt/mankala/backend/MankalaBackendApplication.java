package dev.dt.mankala.backend;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Bean;

import java.util.Optional;

@SpringBootApplication
public class MankalaBackendApplication {


    public static void main(String[] args) {
        SpringApplication.run(MankalaBackendApplication.class, args);
    }

    @Bean
    public OpenAPI customOpenAPI(@Autowired(required = false) BuildProperties buildProperties) {
        String version = null;
        String build = null;
        String name = "";
        String description = "";

        if (buildProperties != null) {
            build = Optional.ofNullable(buildProperties.get("git.commit.id.abbrev")).orElse("latest");
            version = Optional.ofNullable(buildProperties.getVersion()).orElse("0");
            name = buildProperties.getName();
            description = buildProperties.get("build.description");
        }
        String finalVersion = version + "." + build;
        return new OpenAPI()
                .info(new Info()
                        .title(name)
                        .version(finalVersion)
                        .description(description)
                );
    }


}
