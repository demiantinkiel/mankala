package dev.dt.mankala.backend.rest;

import dev.dt.mankala.api.error.MankalaException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
@Slf4j
public class ErrorHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler({MankalaException.class})
    public ResponseEntity<Object> handleException(RuntimeException ex, WebRequest request) {
        HttpStatus errorCode;
        switch (((MankalaException) ex).getType()) {

            case BAD_INPUT:
                errorCode = HttpStatus.BAD_REQUEST;
                break;
            case BAD_STATE:
                errorCode = HttpStatus.CONFLICT;
                break;
            case INTERNAL:
            default:
                errorCode = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        if (log.isTraceEnabled())
            log.error("Exception while {}", request, ex);
        return new ResponseEntity<>(ex.getMessage(), new HttpHeaders(), errorCode);
    }
}
