package dev.dt.mankala.backend;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "mancala.game")
@Configuration
public class MancalaGameConfig {
    int stonesPerPit;
    int pitsPerPlayer;

    public int getStonesPerPit() {
        return stonesPerPit;
    }

    public void setStonesPerPit(int stonesPerPit) {
        this.stonesPerPit = stonesPerPit;
    }

    public int getPitsPerPlayer() {
        return pitsPerPlayer;
    }

    public void setPitsPerPlayer(int pitsPerPlayer) {
        this.pitsPerPlayer = pitsPerPlayer;
    }
}
