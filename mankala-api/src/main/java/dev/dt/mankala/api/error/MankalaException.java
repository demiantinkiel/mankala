package dev.dt.mankala.api.error;

public class MankalaException extends RuntimeException {

    private final ErrorType type;

    public MankalaException(String message, Throwable cause, ErrorType type) {
        super(message, cause);
        this.type = type;
    }

    public ErrorType getType() {
        return type;
    }

    public enum ErrorType {
        INTERNAL, BAD_INPUT, BAD_STATE
    }

}
