package dev.dt.mankala.api.data;

import java.util.Arrays;
import java.util.Set;

public class Game {
    private Set<Player> players;
    private int[] board = new int[14];
    private Player currentPlayer;
    private boolean ended;
    private Player winner;

    public Set<Player> getPlayers() {
        return players;
    }

    public void setPlayers(Set<Player> players) {
        this.players = players;
    }

    public int[] getBoard() {
        return board;
    }

    public void setBoard(int[] board) {
        this.board = board;
    }


    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean isEnded() {
        return ended;
    }

    public void setEnded(boolean ended) {
        this.ended = ended;
    }

    public Player getWinner() {
        return winner;
    }

    public void setWinner(Player winner) {
        this.winner = winner;
    }

    public int[] getPitsForPlayer(Player player) {
        return Arrays.copyOfRange(board, player.getStartPit(), player.getEndPit());
    }

    public void remainingStonesToMancala(Player player) {
        int remaining = Arrays.stream(getPitsForPlayer(player)).sum();
        int total = getMancalaForPlayer(player) + remaining;
        Arrays.fill(board, player.getStartPit(), player.getEndPit() , 0);
        board[player.getEndPit()] = total;
    }

    public int getMancalaForPlayer(Player player) {
        return board[player.getEndPit()];
    }

    @Override
    public String toString() {
        return "Game{" +
                "players=" + players +
                ", board=" + Arrays.toString(board) +
                ", currentPlayer=" + currentPlayer +
                '}';
    }
}
