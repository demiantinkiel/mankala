package dev.dt.mankala.api.data;

import java.util.Objects;

public class Player {
    private String name;
    private int startPit;
    private int endPit;

    public Player(String name) {
        this.name = name;
    }

    public Player() {
        //default constructor for (de)serialization
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStartPit() {
        return startPit;
    }

    public void setStartPit(int startPit) {
        this.startPit = startPit;
    }

    public int getEndPit() {
        return endPit;
    }

    public void setEndPit(int endPit) {
        this.endPit = endPit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Player)) return false;
        Player player = (Player) o;
        return getName().equals(player.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", startPit=" + startPit +
                ", endPit=" + endPit +
                '}';
    }
}
