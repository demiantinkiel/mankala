# Interview project

Project for interview process of Demian Tinkiel. Implementation of [Mancala game](https://en.wikipedia.org/wiki/Mancala)

This projects uses [google's](https://google.github.io/styleguide/javaguide.html) checkstyle rules. Uses
the [editorconfig](.editorconfig) file to keep consistency.

## Requirements

* Docker
* Maven
* JDK 1.8

## Building

```bash
mvn clean install
```

**Note:** This will run tests and generate container images for both frontend and backend modules. Testing steps can be
skipped with `-DskipTests`. Container image generation can be skipped with `-Dspring-boot.build-image.skip=true`

## Running

After bulding the project, from the project's root folder, execute:

```
docker-compose up
```

## Technologies

* Spring
* Java
* Vaadin

## Architecture

![component diagram](doc/components.png)

![sequence diagram](doc/sequence.png)

### Reference Documentation

For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.1/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.1/maven-plugin/reference/html/#build-image)
* [Cloud LoadBalancer](https://cloud.spring.io/spring-cloud-static/spring-cloud-commons/current/reference/html/#spring-cloud-loadbalancer)
* [Vaadin](https://vaadin.com/spring)
* [Spring Configuration Processor](https://docs.spring.io/spring-boot/docs/2.4.1/reference/htmlsingle/#configuration-metadata-annotation-processor)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.4.1/reference/htmlsingle/#using-boot-devtools)
* [vaadin components](https://vaadin.com/components)
* [vaadin resources loading](https://vaadin.com/docs/v14/flow/importing-dependencies/tutorial-ways-of-importing.html)
